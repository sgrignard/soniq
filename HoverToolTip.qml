import QtQuick 2.0
import QtQuick.Controls 2.15

MouseArea {
    property alias text: tooltip.text
    anchors.fill: parent
    id: mouseArea
    hoverEnabled: true
    onPressed: mouse.accepted = false

    ToolTip {
        id: tooltip
        visible: mouseArea.containsMouse
        delay: soniqStyle.toolTipDelay
    }
}
