#ifndef PLAYLISTCONTROLLER_H
#define PLAYLISTCONTROLLER_H

#include <QObject>
#include <QMediaPlayer>
#include <QPointer>

#include "album.h"

class QNetworkAccessManager;
class QNetworkReply;
class PlaylistModel;
class PlaylistSelectionModel;
class SubsonicApi;

class PlaylistController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(PlaybackMode playbackMode READ playbackMode WRITE setPlaybackMode NOTIFY playbackModeChanged)
    Q_PROPERTY(int songCount READ songCount NOTIFY songCountChanged)
    Q_PROPERTY(int playlistPosition READ playlistPosition WRITE setPlaylistPosition NOTIFY playlistPositionChanged)
    Q_PROPERTY(QString streamFormat READ streamFormat WRITE setStreamFormat NOTIFY streamFormatChanged)
public:

    enum class PlaybackMode {
        Sequential,
        Loop,
        Random
    };
    Q_ENUM(PlaybackMode);

    explicit PlaylistController(QObject *parent = nullptr);

    void setApi(SubsonicApi* api);

    void setNetworkAccessManager(QNetworkAccessManager* manager);

    QMediaPlayer* player() const;

    PlaybackMode playbackMode() const;

    Q_INVOKABLE int firstIndex() const;

    int songCount() const;

    int playlistPosition() const;

    QString streamFormat() const;

    QByteArray save() const;

    void load(const QByteArray& bytes);

    PlaylistModel* model();

    PlaylistSelectionModel* selectionModel();

public slots:

    void setPlaybackMode(PlaybackMode mode);

    void setPlaylistPosition(int position);

    void setStreamFormat(const QString& format);

    void goNext();

    void goPrevious();

    void goFirst();

    void startAsap();

signals:

    void playbackModeChanged(PlaybackMode mode);

    void songCountChanged(int count);

    void playlistPositionChanged(int position);

    void streamFormatChanged(const QString& format);


private slots:

    void onPlayerStatusChanged(QMediaPlayer::MediaStatus status);

    void onCurrentRowChanged(const QModelIndex& current);

    void changeCurrentSong(int offset);

    void updatePlaylistIndexes();

    void goFirstAndPlay();

private:

    QPointer<PlaylistModel> m_model;
    QPointer<PlaylistSelectionModel> m_selectionModel;
    QPointer<SubsonicApi> m_api;
    QPointer<QMediaPlayer> m_player;
    QPointer<QNetworkAccessManager> m_networkAccessManager;
    QPointer<QNetworkReply> m_currentReply;
    Song m_currentSong;
    PlaybackMode m_playbackMode;
    QList<int> m_playlistIndexes;
    int m_playlistPosition;
    QString m_streamFormat;

};

#endif // PLAYLISTCONTROLLER_H
