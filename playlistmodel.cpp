#include <QMap>
#include <QMediaPlayer>
#include <QMetaProperty>
#include <QNetworkReply>
#include <QStringList>

#include "playlistmodel.h"

static const int COLUMN_COUNT = Song::staticMetaObject.propertyCount() - Song::staticMetaObject.propertyOffset();
static const QMap<QString, QString> HEADER_DATA = {
    {"id",          QT_TRANSLATE_NOOP("PlaylistModel", "Id")},
    {"title",       QT_TRANSLATE_NOOP("PlaylistModel", "Title")},
    {"album",       QT_TRANSLATE_NOOP("PlaylistModel", "Album")},
    {"artist",      QT_TRANSLATE_NOOP("PlaylistModel", "Artist")},
    {"duration",    QT_TRANSLATE_NOOP("PlaylistModel", "Duration")},
};

PlaylistModel::PlaylistModel(QObject* parent)
    : QAbstractTableModel(parent)
{
}

void PlaylistModel::setApi(SubsonicApi *api)
{
    m_api = api;
    QObject::connect(api, &SubsonicApi::urlChanged, this, &PlaylistModel::clear);
    QObject::connect(api, &SubsonicApi::credentialsChanged, this, &PlaylistModel::clear);
}

int PlaylistModel::rowCount(const QModelIndex&) const
{
    return m_songs.size();
}

int PlaylistModel::columnCount(const QModelIndex &) const
{
    return COLUMN_COUNT;
}

QVariant PlaylistModel::data(const QModelIndex& index, int role) const
{
    if (index.row()<0 || index.row()>=m_songs.size())
        return QVariant();
    if (index.column()<0 || index.column()>=COLUMN_COUNT)
        return QVariant();
    const Song& song= m_songs[index.row()];
    switch (role) {
    case Qt::DisplayRole:
        return Song::staticMetaObject.property(index.column()).readOnGadget(&song);
    case int(Roles::SongRole):
        return QVariant::fromValue(song);
    case int(Roles::PropertyNameRole):
        return QString(Song::staticMetaObject.property(index.column()).name());
    case int(Roles::ColumnRole):
        return index.column();
    case int(Roles::RowRole):
        return index.row();
    }
    return QVariant();
}

QHash<int, QByteArray> PlaylistModel::roleNames() const
{
    QHash<int, QByteArray> names = QAbstractItemModel::roleNames();
    names[int(Roles::SongRole)] = "song";
    names[int(Roles::PropertyNameRole)] = "propertyName";
    names[int(Roles::ColumnRole)] = "column";
    names[int(Roles::RowRole)] = "row";
    return names;
}

QVariant PlaylistModel::headerData(int section, Qt::Orientation orientation , int role) const
{
    if (section<0 || (orientation==Qt::Horizontal && section>COLUMN_COUNT) || (orientation==Qt::Vertical && section>=m_songs.size()))
        return QVariant();

    if (Qt::Vertical == orientation && role==Qt::DisplayRole) return section + 1;
    switch (role) {
    case Qt::DisplayRole:
        return HEADER_DATA.value(Song::staticMetaObject.property(section).name());
    case int(Roles::PropertyNameRole):
        return QString(Song::staticMetaObject.property(section).name());
    }
    return QVariant();
}

void PlaylistModel::clear()
{
    beginRemoveRows(QModelIndex(), 0, m_songs.size() - 1);
    m_songs.clear();
    endRemoveRows();
    emit itemCountChanged(0);
}

void PlaylistModel::addAlbum(const QString &albumId)
{
    QNetworkReply* reply = m_api->getAlbum(albumId);
    QObject::connect(reply, &QNetworkReply::finished, [this, reply]() {
        Album album = m_api->parseAlbumReply(reply);
        beginInsertRows(QModelIndex(), m_songs.size(), m_songs.size() + album.m_songs.size() - 1);
        m_songs += album.m_songs;
        endInsertRows();
        emit itemCountChanged(m_songs.size());
    });
}

Song PlaylistModel::song(int row) const
{
    if (row<0 || row>=m_songs.size()) return Song();
    return m_songs[row];
}

int PlaylistModel::itemCount() const
{
    return rowCount(QModelIndex());
}

bool PlaylistModel::removeRows(int row, int count, const QModelIndex &parent)
{
    count = std::min(count, m_songs.size() - row);
    beginRemoveRows(parent, row, row + count - 1);
    m_songs = m_songs.mid(0, row) + m_songs.mid(row + count);
    endRemoveRows();
    return true;
}

QModelIndex PlaylistModel::root() const
{
    return QModelIndex();
}

void PlaylistModel::setSongs(const SongList &songs)
{
    clear();
    beginInsertRows(QModelIndex(), m_songs.size(), m_songs.size() + songs.size() - 1);
    m_songs += songs;
    endInsertRows();
}
