import QtQuick 2.15

ColorImage {
    required property var player
    property real volumeIncrement: 10
    color: palette.text
    source: {
        if (player.muted) return "qrc:///images/loudspeaker-muted.svg"
        const index = Math.floor(Math.max(0, player.volume - 1) / 25)
        return `qrc:///images/loudspeaker-${index}.svg`
    }

    MouseArea {
        anchors.fill: parent
        onClicked: player.muted = !player.muted
        onWheel: {
            const volume = player.volume + (wheel.angleDelta.y > 0 ? volumeIncrement : -volumeIncrement)
            if (volume < 0 || volume > 100) return
            player.volume = volume
        }
    }

    Shortcut {
        sequence: "M"
        onActivated: player.muted = !player.muted
    }

    HoverToolTip {
        text: player.muted ? qsTr("Muted") : qsTr("Volume at %1%").arg(player.volume)
    }
}
