import QtQuick 2.0
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import Soniq 1.0

RowLayout {
    required property var song
    required property PlaylistController controller
    required property QMediaPlayer player
    id: info
    Image {
        source: song ? api.coverArtUrl(song.coverArt, parent.height) : "qrc:///images/alto.svg"
        enabled: !Boolean(song)
        Layout.preferredWidth: parent.height
        Layout.preferredHeight: parent.height
    }
    ColumnLayout {
        Layout.alignment: Qt.AlignBottom
        PlayerControls {
            player: info.player
            controller: info.controller
        }
        SongInfo { song: info.song }
        ProgressBar {
            id: progress
            Layout.fillWidth: true
            from: 0
            to: song ? song.duration : 0
            // player.position is in ms
            value: 0.001*player.position
            background: Rectangle {
                color: palette.dark
                implicitHeight: 10
            }
            contentItem: Item {
                implicitHeight: 8
                Rectangle {
                    color: palette.highlight
                    height: parent.height
                    width: progress.visualPosition * parent.width
                }
            }
        }
    }
}
