import QtQuick 2.0
import QtQuick.Controls 2.15

ToolButton {
    id: control
    property int size: soniqStyle.iconSize
    display: AbstractButton.IconOnly
    icon.height: size
    icon.width: size
    icon.color: enabled ? palette.text: disabledPalette.text
    HoverToolTip {
        text: control.text
    }
}
