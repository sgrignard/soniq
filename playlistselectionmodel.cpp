#include <QDebug>

#include "playlistselectionmodel.h"

PlaylistSelectionModel::PlaylistSelectionModel(QAbstractItemModel* model)
    : QItemSelectionModel(model)
{
}

void PlaylistSelectionModel::selectRow(int row, bool clear)
{
    QItemSelectionModel::SelectionFlags flags = QItemSelectionModel::Rows;
    flags |= clear ? QItemSelectionModel::ClearAndSelect : QItemSelectionModel::Select;
    QItemSelection rowSelection(
        model()->index(row, 0),
        model()->index(row, model()->columnCount() - 1)
    );
    select(rowSelection, flags);
}

void PlaylistSelectionModel::setCurrentRow(int row)
{
    setCurrentIndex(
        model()->index(row, 0),
        QItemSelectionModel::SelectCurrent|QItemSelectionModel::Rows
    );
}
