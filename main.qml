import QtQml.Models 2.15
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15

import Soniq 1.0

Window {
    id: window
    width: 1024
    height: 768
    visible: true
    color: palette.window
    title: qsTr("Soniq")
    ColumnLayout {
        anchors.fill: parent
        RowLayout {
            Layout.margins: 5
            SongProgress {
                song: playlistModel.data(playlistSelectionModel.currentIndex, PlaylistModel.SongRole)
                player: mediaPlayer
                controller: playlistController
                Layout.maximumHeight: 100
            }
            IconButton {
                id: showSettings
                action: settingsWindow.showAction
            }
        }
        SplitView {
            id: views
            spacing: 10
            handle: Rectangle {
                implicitWidth: 5
                color: SplitHandle.pressed ? palette.dark : palette.mid
                HoverHandler {
                    cursorShape: Qt.SizeHorCursor
                }
            }

            Layout.fillHeight: true
            Layout.fillWidth: true
            AlbumView {
                id: albumView
                model: albumModel
                onAddAlbumClicked: (album) => { playlistModel.addAlbum(album.id) }
                onPlayAlbumClicked: (album) => {
                    playlistModel.clear()
                    playlistController.startAsap()
                    playlistModel.addAlbum(album.id)
                }
                SplitView.preferredWidth: 0.67*window.width
                SplitView.fillHeight: true
                KeyNavigation.tab: playlist
            }
            Frame {
                id: playlist
                background: BackgroundRectangle {}
                SplitView.fillWidth: true
                SplitView.fillHeight: true
                ColumnLayout {
                    anchors.fill: parent
                    PlaylistControls {
                        controller: playlistController
                        model: playlistModel
                    }
                    PlaylistView {
                        model: playlistModel
                        selectionModel: playlistSelectionModel
                        onSongDoubleClicked: (song, index) => {
                            playlistSelectionModel.setCurrentRow(index)
                            mediaPlayer.play()
                        }
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                    }
                }
                KeyNavigation.backtab: albumView
            }
        }
    }

    Settings {
        id: settingsWindow
        modal: true
        anchors.centerIn: parent
        visible: settings.subsonicUrl === ""
    }

    ErrorPopup {
        id: errorPopup
        visible: false
        topMargin: 0.15*parent.height - height
        x: Math.round((parent.width - width) / 2)
        Connections {
            target: api
            function onErrorOccurred (error) {
                errorPopup.message = error.message
                errorPopup.details = error.details
                errorPopup.open()
            }
        }
    }

    SystemPalette {
        id: palette
    }
    SystemPalette {
        id: disabledPalette
        colorGroup: SystemPalette.Disabled
    }
    Style {
        id: soniqStyle
    }
}
