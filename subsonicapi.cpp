#include <QDomDocument>
#include <QFileInfo>
#include <QMap>
#include <QMetaObject>
#include <QMetaProperty>
#include <QNetworkReply>
#include <QUrlQuery>

#include "album.h"
#include "settings.h"

#include "subsonicapi.h"

static const QMap<SubsonicApi::AlbumListType, QString> ALBUM_LIST_TYPE_QUERY({
    {SubsonicApi::AlbumListType::NEWEST, "newest"},
    {SubsonicApi::AlbumListType::ALPHABETICAL_BY_NAME, "alphabeticalByName"},
    {SubsonicApi::AlbumListType::RANDOM, "random"}
});

#define SUBSONIC_OK "ok"
#define SUBSONIC_FAILED "failed"

template<class T>
T createObject(const QDomElement& element)
{
    QMetaObject meta = T::staticMetaObject;
    T object;
    for (int i=0; i<meta.propertyCount(); ++i) {
        QMetaProperty property = meta.property(i);
        property.writeOnGadget(&object, element.attribute(property.name()));
    }
    return object;
}

template<class T> QList<T> getList(const QDomDocument& document, const QString& elementTagName)
{
    QDomElement root = document.documentElement();
    QDomNodeList elements = root.firstChildElement().elementsByTagName(elementTagName);
    QList<T> objects;
    for (int i=0; i<elements.size(); ++i)
        objects.append(createObject<T>(elements.at(i).toElement()));
    return objects;
}

SubsonicApi::SubsonicApi(QNetworkAccessManager* manager)
    : QObject(manager)
    , m_manager(manager)
{
}

QUrl SubsonicApi::url() const
{
    return m_url;
}

QNetworkAccessManager* SubsonicApi::manager() const
{
    return m_manager;
}

AlbumList SubsonicApi::parseAlbumListReply(QNetworkReply* reply)
{
    return getList<Album>(getDocument(reply), "album");
}

Album SubsonicApi::parseAlbumReply(QNetworkReply* reply)
{
    QDomDocument document = getDocument(reply);
    QDomElement root = document.documentElement();
    QDomElement albumElement = root.firstChildElement("album");
    Album album = createObject<Album>(albumElement);
    QDomNodeList songElements = albumElement.elementsByTagName("song");
    for (int i=0; i<songElements.size(); ++i) {
        QDomElement songElement = songElements.at(i).toElement();
        Song s = createObject<Song>(songElement);
        album.m_songs.append(s);
    }
    return album;
}

Artist SubsonicApi::parseArtistReply(QNetworkReply *reply)
{
    QDomDocument document = getDocument(reply);
    QDomElement root = document.documentElement();
    QDomElement artistElement = root.firstChildElement("artist");
    Artist artist = createObject<Artist>(artistElement);
    QDomNodeList albumElements = artistElement.elementsByTagName("album");
    for (int i=0; i<albumElements.size(); ++i) {
        QDomElement albumElement = albumElements.at(i).toElement();
        Album album = createObject<Album>(albumElement);
        artist.m_albums.append(album);
    }
    return artist;
}

SearchResult SubsonicApi::parseSearchReply(QNetworkReply *reply)
{
    QDomDocument document = getDocument(reply);
    return SearchResult{getList<Album>(document, "album"), getList<Artist>(document, "artist")};
}

QString SubsonicApi::coverArtUrl(const QString& coverArtId, int size) const
{
    QUrlQuery query = {
        {"id", coverArtId},
        {"size", size ? QString::number(size) : QString()}
    };
    auto u = getUrl("getCoverArt", query).toString();
    qDebug() << "coverArtUrl" << u;
    return u;
}

QString SubsonicApi::streamUrl(const QString &fileId, const QString& format) const
{
    QUrl url = getUrl("stream", {{"id", fileId}, {"format", format}});
    return url.toString();
}

void SubsonicApi::setUrl(const QUrl& url)
{
    if (url == m_url) return;
    m_url = url;
    emit urlChanged(url);
}

void SubsonicApi::setCredentials(const QString &user, const QString &password)
{
    if (user == m_user && password == m_password) return;
    m_user = user;
    m_password = password;
    emit credentialsChanged(user, password);
}

QNetworkReply* SubsonicApi::getAlbumList2(
        AlbumListType type,
        uint32_t size,
        uint32_t offset,
        uint32_t fromYear,
        uint32_t toYear,
        const QString& genre) const
{
    QUrlQuery query = {
        {"type", ALBUM_LIST_TYPE_QUERY[type]},
        {"size", size ? QString::number(size) : QString()},
        {"offset", offset ? QString::number(offset) : QString()},
        {"fromYear", fromYear ? QString::number(fromYear) : QString()},
        {"toYear", toYear ? QString::number(toYear) : QString()},
        {"genre", genre}
    };
    return get("getAlbumList2", query);
}

QNetworkReply *SubsonicApi::getSearch3(
        const QString &query,
        int artistCount,
        int artistOffset,
        int albumCount,
        int albumOffset,
        int songCount,
        int songOffset) const
{
    QUrlQuery urlQuery = {
        {"query", query},
        {"artistCount", artistCount ? QString::number(artistCount) : QString()},
        {"artistOffset", artistOffset? QString::number(artistOffset) : QString()},
        {"albumCount", albumCount ? QString::number(albumCount) : QString()},
        {"albumOffset", albumOffset ? QString::number(albumOffset) : QString()},
        {"songCount", songCount ? QString::number(songCount) : QString()},
        {"songOffset", songOffset ? QString::number(songOffset) : QString()},
    };
    return get("search3", urlQuery);
}

QNetworkReply* SubsonicApi::getAlbum(const QString& albumId) const
{
    return get("getAlbum", {{"id", albumId}});
}

QNetworkReply *SubsonicApi::getArtist(const QString &artistId) const
{
    return get("getArtist", {{"id", artistId}});
}

QVariant SubsonicApi::album(const QString& albumId) const
{
    return QVariant::fromValue(getAlbum(albumId));
}

QString SubsonicApi::songId(const QUrl &streamUrl) const
{
    assert(streamUrl.hasQuery());
    QUrlQuery query(streamUrl);
    return query.queryItemValue("id");
}

QUrl SubsonicApi::getUrl(const QString& path, const QUrlQuery& query) const
{
    QUrl u = url();
    u.setPath(u.path() + "/rest/" + path);
    QUrlQuery q = query;
    q.addQueryItem("u", m_user);
    q.addQueryItem("p", m_password);
    q.addQueryItem("v", "1.10.2");
    q.addQueryItem("c", "Soniq");
    u.setQuery(q);
    return u;
}

QNetworkReply* SubsonicApi::get(const QString& path, const QUrlQuery& query) const
{
    QUrl url = getUrl(path, query);
    QNetworkReply* reply = m_manager->get(QNetworkRequest(url));
    QObject::connect(reply, &QNetworkReply::finished, reply, &QNetworkReply::deleteLater);
    QObject::connect(reply, &QNetworkReply::errorOccurred, [this, reply]() {
        QDomDocument doc = getDocument(reply);
        if (doc.isNull()) return;
        SubsonicApiError error{
            reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(),
            tr("Got an error from the server"),
            SubsonicApiError::ErrorCode(doc.firstChildElement().firstChildElement("error").attribute("code").toInt()),
            reply->errorString()
        };
        emit errorOccurred(error);
    });
    return reply;
}

QDomDocument SubsonicApi::getDocument(QNetworkReply *reply) const
{
    int httpStatus = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    if (httpStatus != 200) {
        emit errorOccurred(SubsonicApiError{
            httpStatus,
            tr("Got an error from the server"),
            SubsonicApiError::ErrorCode::HttpStatusNotOk,
            reply->errorString()
        });
        return QDomDocument();
    };

    QDomDocument document;
    QString message;
    int line, column;
    if (!document.setContent(reply, true, &message, &line, &column)) {
        emit errorOccurred(SubsonicApiError{
            httpStatus,
            tr("Malformed XML document at line %1, column %2").arg(line, column),
            SubsonicApiError::ErrorCode::MalformedXMLDocument,
            message
        });
        return QDomDocument();
    }

    QDomElement root = document.documentElement();
    if (root.tagName() != "subsonic-response") {
        emit errorOccurred(SubsonicApiError{
            httpStatus,
            tr("Not a subsonic response"),
            SubsonicApiError::ErrorCode::NotSubsonicResponse,
            QString("the root element is a %1 tag, not subsonic-response").arg(root.tagName())
        });
        return QDomDocument();
    }

    if (root.attribute("status") != SUBSONIC_OK) {
        auto errorElement = root.firstChildElement("error");
        SubsonicApiError error{
            httpStatus,
            tr("Subsonic API error"),
            SubsonicApiError::ErrorCode(errorElement.attribute("code").toInt()),
            errorElement.attribute("message")
        };
        emit errorOccurred(error);
        return QDomDocument();
    }
    return document;
}

MultipleNetworkReplies::MultipleNetworkReplies()
{
}

void MultipleNetworkReplies::addReply(QNetworkReply *reply)
{
    m_replies.append(reply);
    QObject::connect(reply, &QNetworkReply::finished, this, &MultipleNetworkReplies::replyFinished);
    // we'll take care of deleting the reply when all have finished.
    bool ok = QObject::disconnect(reply, &QNetworkReply::finished, reply, &QNetworkReply::deleteLater);
    assert(ok);
    QObject::connect(this, &MultipleNetworkReplies::finished, reply, &QNetworkReply::deleteLater);
}

QList<QNetworkReply *> MultipleNetworkReplies::finishedReplies() const
{
    QList<QNetworkReply*> replies;
    for (auto r: m_replies) {
        if (r->isFinished()) replies.append(r);
    }
    return replies;
}

void MultipleNetworkReplies::replyFinished()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    assert(reply);

    if (std::all_of(m_replies.begin(), m_replies.end(), [](auto reply) { return reply->isFinished(); })) {
        emit finished();
        deleteLater();
    }
}
