#ifndef ALBUM_H
#define ALBUM_H

#include <QDateTime>
#include <QObject>
#include <QQmlEngine>
#include <QUrl>

struct Song
{
    Q_GADGET
    Q_PROPERTY(QString id MEMBER m_id)
    Q_PROPERTY(QString title MEMBER m_title)
    Q_PROPERTY(QString album MEMBER m_album)
    Q_PROPERTY(QString artist MEMBER m_artist)
    Q_PROPERTY(int duration MEMBER m_duration)
    Q_PROPERTY(QString coverArt MEMBER m_coverArtId)
    //QML_ELEMENT

public:

    inline bool operator ==(const Song& other) const {
        return (
            m_id==other.m_id &&
            m_title==other.m_title &&
            m_album==other.m_album &&
            m_artist==other.m_artist &&
            m_duration==other.m_duration &&
            m_coverArtId==other.m_coverArtId
        );
    }

    QString m_id;
    QString m_title;
    QString m_album;
    QString m_artist;
    int m_duration;
    QString m_coverArtId;
};

using SongList = QList<Song>;

struct Album
{
    Q_GADGET
    Q_PROPERTY(QString id MEMBER m_id)
    Q_PROPERTY(QString name MEMBER m_name)
    Q_PROPERTY(QString title MEMBER m_title)
    Q_PROPERTY(QString coverArt MEMBER m_coverArtId)
    Q_PROPERTY(SongList songs MEMBER m_songs)
    //QML_ELEMENT

public:

    inline bool operator ==(const Album& other) const {
        return (
            m_id == other.m_id &&
            m_name == other.m_name &&
            m_title == other.m_title &&
            m_coverArtId == other.m_coverArtId &&
            m_songCount == other.m_songCount &&
            m_created == other.m_created &&
            m_duration == other.m_duration &&
            m_artistName == other.m_artistName &&
            m_artistId == other.m_artistId &&
            m_songs == other.m_songs
        );
    }

    QString m_id;
    QString m_name;
    QString m_title;
    QString m_coverArtId;
    uint32_t m_songCount;
    QDateTime m_created;
    uint32_t m_duration;
    QString m_artistName;
    uint32_t m_artistId;
    SongList m_songs;

};

using AlbumList = QList<Album>;

struct Artist {
    Q_GADGET
    Q_PROPERTY(QString id MEMBER m_id)
    Q_PROPERTY(QString name MEMBER m_name)
    Q_PROPERTY(QString coverArt MEMBER m_coverArtId)
    Q_PROPERTY(int albumCount MEMBER m_albumCount)
    Q_PROPERTY(AlbumList albums MEMBER m_albums)
public:
    QString m_id;
    QString m_name;
    QString m_coverArtId;
    int m_albumCount;
    AlbumList m_albums;
};

using ArtistList = QList<Artist>;

Q_DECLARE_METATYPE(Song);
Q_DECLARE_METATYPE(Album);
Q_DECLARE_METATYPE(Artist);

#endif // ALBUM_H
