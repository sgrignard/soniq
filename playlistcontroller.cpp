#include <algorithm>
#include <random>

#include <QDataStream>
#include <QMediaContent>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QRandomGenerator>
#include <QTimer>

#include "album.h"
#include "playlistmodel.h"
#include "playlistselectionmodel.h"
#include "subsonicapi.h"

#include "playlistcontroller.h"
#include "playlistcontroller.h"

template<class T>
void writeObject(const T& object, QDataStream& stream)
{
    QMetaObject meta = T::staticMetaObject;
    stream << meta.propertyCount();
    for (int i=0; i<meta.propertyCount(); ++i) {
        QMetaProperty property = meta.property(i);
        stream << QString(property.name()) << property.readOnGadget(&object);
    }
}

template<class T>
void readObject(T& object, QDataStream& stream)
{
    QMetaObject meta = T::staticMetaObject;
    QVariant value;
    int propertyCount;
    QString name;
    stream >> propertyCount;
    for (int i=0; i<propertyCount; ++i) {
        stream >> name >> value;
        QMetaProperty property = meta.property(meta.indexOfProperty(name.toStdString().c_str()));
        if (!property.isValid()) continue;
        property.writeOnGadget(&object, value);
    }
}


PlaylistController::PlaylistController(QObject *parent)
    : QObject(parent)
    , m_model(new PlaylistModel(this))
    , m_selectionModel(new PlaylistSelectionModel(m_model))
    , m_player(new QMediaPlayer(this))
    , m_playbackMode(PlaybackMode::Sequential)
    , m_playlistPosition(-1)
{
    QObject::connect(m_player, &QMediaPlayer::mediaStatusChanged, this, &PlaylistController::onPlayerStatusChanged);
    QObject::connect(m_player, &QMediaPlayer::positionChanged, [this](qint64 position) {
       if (m_currentSong.m_duration > 0 && position >= m_currentSong.m_duration*1000) goNext();
    });

    QObject::connect(m_selectionModel, &PlaylistSelectionModel::currentRowChanged, this, &PlaylistController::onCurrentRowChanged);

    auto f = [this]() {
        updatePlaylistIndexes();
        auto rowCount = m_model->rowCount(QModelIndex());
        emit songCountChanged(rowCount);
        if (m_playlistPosition>=rowCount) setPlaylistPosition(-1);
    };

    QObject::connect(m_model, &QAbstractItemModel::rowsInserted, f);
    QObject::connect(m_model, &QAbstractItemModel::rowsRemoved, f);
}

void PlaylistController::setApi(SubsonicApi *api)
{
    m_api = api;
    m_model->setApi(api);
}

void PlaylistController::setNetworkAccessManager(QNetworkAccessManager *manager)
{
    m_networkAccessManager = manager;
}

QMediaPlayer *PlaylistController::player() const
{
    return m_player;
}

PlaylistController::PlaybackMode PlaylistController::playbackMode() const
{
    return m_playbackMode;
}

int PlaylistController::firstIndex() const
{
    return m_playlistIndexes[0];
}

int PlaylistController::songCount() const
{
    return m_selectionModel->model()->rowCount();
}

int PlaylistController::playlistPosition() const
{
    return m_playlistPosition;
}

QString PlaylistController::streamFormat() const
{
    return m_streamFormat;
}

void PlaylistController::setPlaybackMode(PlaylistController::PlaybackMode mode)
{
    if (mode == m_playbackMode) return;
    m_playbackMode = mode;
    updatePlaylistIndexes();
    emit playbackModeChanged(mode);
}

void PlaylistController::setPlaylistPosition(int position)
{
    if (position == m_playlistPosition) return;

    if (position < 0 || position > m_playlistIndexes.size() - 1) {
        m_playlistPosition = -1;
        m_selectionModel->setCurrentRow(-1);
    } else {
        m_playlistPosition = position;
        m_selectionModel->setCurrentRow(m_playlistIndexes[position]);
    }
    emit playlistPositionChanged(m_playlistPosition);
}

void PlaylistController::setStreamFormat(const QString &format)
{
    if (format == m_streamFormat) return;
    m_streamFormat = format;
    emit streamFormatChanged(format);
}

void PlaylistController::goNext()
{
    changeCurrentSong(1);
}

void PlaylistController::goPrevious()
{
    changeCurrentSong(-1);
}

void PlaylistController::goFirst()
{
    setPlaylistPosition(0);
}

void PlaylistController::startAsap()
{
    QMetaObject::Connection connection = QObject::connect(
        m_model,
        &QAbstractItemModel::rowsInserted,
        this,
        &PlaylistController::goFirstAndPlay,
        Qt::UniqueConnection
    );
    QTimer::singleShot(500, [connection]() {
        QObject::disconnect(connection);
    });
}

void PlaylistController::onPlayerStatusChanged(QMediaPlayer::MediaStatus status)
{
    //qDebug() << status << m_player->errorString() << m_player->currentMedia().request().url();
    if (QMediaPlayer::MediaStatus::InvalidMedia == status) {
        qDebug() << "Invalid media: " << m_player->errorString();
        if (!m_currentReply.isNull()) m_currentReply->abort();
    }
    if (QMediaPlayer::MediaStatus::NoMedia == status && !m_currentReply.isNull()) {
        m_currentReply->abort();
    }
    if (QMediaPlayer::MediaStatus::EndOfMedia != status) return;
    goNext();
}

void PlaylistController::onCurrentRowChanged(const QModelIndex& current)
{
    for (auto position = 0; position < m_playlistIndexes.size(); ++position)
        if (m_playlistIndexes[position] == current.row()) {
            setPlaylistPosition(position);
            break;
        }
    auto playerState = m_player->state();
    m_currentSong = m_selectionModel->model()->data(current, PlaylistModel::Roles::SongRole).value<Song>();
    QUrl streamUrl(m_api->streamUrl(m_currentSong.m_id, m_streamFormat));
    if (!m_currentReply.isNull()) m_currentReply->abort();
    m_currentReply = m_networkAccessManager->get(QNetworkRequest(streamUrl));
    QObject::connect(m_currentReply, &QNetworkReply::errorOccurred, [](QNetworkReply::NetworkError error) {
       qDebug() << "ERR" << error;
    });
    m_player->setMedia(streamUrl, m_currentReply);
    if (playerState == QMediaPlayer::PlayingState) m_player->play();
}

void PlaylistController::changeCurrentSong(int offset)
{
    int newIndex = m_playlistPosition + offset;
    // Loop index if mode is Loop
    if (PlaybackMode::Loop == m_playbackMode) {
        newIndex = newIndex % m_playlistIndexes.size();
    }
    // Outside list => stop everything
    if (newIndex < 0 || newIndex >= m_playlistIndexes.size()) {
        m_playlistPosition = -1;
        m_selectionModel->setCurrentRow(-1);
        return;
    }

    m_playlistPosition = newIndex;
    m_selectionModel->setCurrentRow(m_playlistIndexes[newIndex]);
    emit playlistPositionChanged(m_playlistPosition);
}

void PlaylistController::updatePlaylistIndexes() {
    m_playlistIndexes.clear();
    for (auto i = 0; i < m_selectionModel->model()->rowCount(); ++i)
        m_playlistIndexes.append(i);
    if (PlaybackMode::Random == m_playbackMode) {
        auto seed = std::rand();
        std::shuffle(
            m_playlistIndexes.begin(),
            m_playlistIndexes.end(),
            std::default_random_engine(seed)
        );
    }
}

void PlaylistController::goFirstAndPlay()
{
    goFirst();
    m_player->play();
}

QByteArray PlaylistController::save() const
{
    QByteArray bytes;
    QDataStream stream(&bytes, QIODevice::WriteOnly);
    stream << m_playlistPosition << m_playbackMode << m_model->itemCount();
    for (auto index=0; index<m_model->itemCount(); ++index)
        writeObject(m_model->song(index), stream);
    return bytes;
}

void PlaylistController::load(const QByteArray &bytes)
{
    QDataStream stream(bytes);
    SongList songs;
    int playlistPosition, itemCount;
    stream >> playlistPosition >> m_playbackMode >> itemCount;
    for (auto index=0; index<itemCount; ++index) {
        Song song;
        readObject(song, stream);
        songs.append(song);
    }
    m_model->setSongs(songs);
    setPlaylistPosition(playlistPosition);
}

PlaylistModel *PlaylistController::model()
{
    return m_model;
}

PlaylistSelectionModel *PlaylistController::selectionModel()
{
    return m_selectionModel;
}
