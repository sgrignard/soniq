import QtQuick 2.0
import QtQuick.Controls 2.15

TextField {
    id: field

    Rectangle {
        border.color: "orange"
        anchors.fill: parent
        visible: !field.acceptableInput
        z: -0.5
    }
}
