#ifndef PLAYLISTSELECTIONMODEL_H
#define PLAYLISTSELECTIONMODEL_H

#include <QItemSelectionModel>

class PlaylistSelectionModel : public QItemSelectionModel
{
    Q_OBJECT
public:
    explicit PlaylistSelectionModel(QAbstractItemModel* model);

    Q_INVOKABLE void selectRow(int row, bool clear = true);

    Q_INVOKABLE void setCurrentRow(int row);
};

#endif // PLAYLISTSELECTIONMODEL_H
