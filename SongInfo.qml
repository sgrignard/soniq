import QtQuick 2.15
import QtQuick.Layouts 1.15

ColumnLayout {
    required property var song
    Text {
        text: song ? song.title : ""
        color: palette.text
        font.bold: true
    }
    Text {
        text: song ? `${song.artist} · ${song.album}` : ""
        color: palette.text
        font.pointSize: soniqStyle.smallFontPointSize
    }
}
