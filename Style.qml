import QtQuick 2.0

QtObject {
    // Font sizes
    property int smallFontPointSize: 8

    // Icon sizes
    property int smallIconSize: 15
    property int iconSize: 35

    // Misc
    property int toolTipDelay: 250

}
