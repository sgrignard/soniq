.pragma library

function getMaxColumnItemWidth(column, view) {
    const children = view.contentItem.children
    let width = 70
    for (let idx = 0; idx < children.length; ++idx) {
        const child = children[idx]
        if (child.column === column)
            width = Math.max(width, child.implicitWidth)
    }
    return width
}

function getMaxRowWidth(view) {
    let rowWidths = []
    while (rowWidths.length < view.rows) {
        rowWidths.push(0)
    }
    const children = view.contentItem.children
    for (let idx = 0; idx < children.length; ++idx) {
        const child = children[idx]
        const currentWidth = rowWidths[child.row]
        rowWidths[child.row] = currentWidth + child.implicitWidth
    }
    return Math.max(...rowWidths)
}
