#include <QQuickStyle>

#include "application.h"

Application::Application(int& argc, char** argv)
    : QGuiApplication(argc, argv)
{

}

void Application::loadSettings()
{
    bool ok;
    Settings s = readSettings(ok);
    updateSettings(QVariant::fromValue(s));
    QObject::connect(this, &Application::aboutToQuit, [this]() {
        writeSettings(m_settings);
    });
}

const Settings *Application::settings() const
{
    return &m_settings;
}

void Application::updateSettings(const QVariant &settings)
{
    m_settings = settings.value<Settings>();
    emit settingsChanged(m_settings);
}
