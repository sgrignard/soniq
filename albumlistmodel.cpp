#include <QNetworkReply>

#include "albumlistmodel.h"

#define FETCH_MORE_COUNT 10

const int AlbumListModel::AlbumRole = Qt::UserRole + 1;

AlbumListModel::AlbumListModel(SubsonicApi* api)
    : QAbstractListModel(api),
      m_api(api),
      m_allAlbumsFetched(false),
      m_fetchPending(false)
{
    QObject::connect(api, &SubsonicApi::urlChanged, this, &AlbumListModel::reload);
    QObject::connect(api, &SubsonicApi::credentialsChanged, this, &AlbumListModel::reload);
}

int AlbumListModel::rowCount(const QModelIndex&) const
{
    return m_albums.size();
}

QVariant AlbumListModel::data(const QModelIndex& index, int role) const
{
    if (index.row()<0 || index.row()>=m_albums.size())
        return QVariant();
    const Album& album = m_albums[index.row()];
    switch (role) {
        case Qt::DisplayRole:
            return album.m_name;
        case AlbumRole:
            return QVariant::fromValue(album);
    }
    return QVariant();
}

QHash<int, QByteArray> AlbumListModel::roleNames() const
{
    QHash<int, QByteArray> names = QAbstractItemModel::roleNames();
    names[AlbumRole] = "album";
    return names;
}

QString AlbumListModel::searchQuery() const
{
    return m_searchQuery;
}

void AlbumListModel::setSearchQuery(const QString &query)
{
    if (query == m_searchQuery) return;
    m_searchQuery = query;
    clear();
    m_allAlbumsFetched = false;
    fetchMore(QModelIndex());
    emit searchQueryChanged(m_searchQuery);
}

bool AlbumListModel::canFetchMore(const QModelIndex &) const
{
    return !(m_allAlbumsFetched || m_fetchPending);
}

void AlbumListModel::fetchMore(const QModelIndex&)
{
    fetchMoreCount(FETCH_MORE_COUNT);
}

void AlbumListModel::fetchMoreCount(int count)
{
    if (!canFetchMore(QModelIndex())) return;
    m_fetchPending = true;
    QNetworkReply* reply;
    if (m_searchQuery.isEmpty()) {
        reply = m_api->getAlbumList2(
          SubsonicApi::AlbumListType::ALPHABETICAL_BY_NAME,
          count,
          m_albums.size()
        );
        QObject::connect(reply, &QNetworkReply::finished, [this, reply, count]() {
            addAlbums(m_api->parseAlbumListReply(reply), count);
        });
    } else {
        reply = m_api->getSearch3(m_searchQuery, count, 0, count, m_albums.size(), 0, 0);
        QObject::connect(reply, &QNetworkReply::finished, [this, reply, count]() {
            SearchResult result = m_api->parseSearchReply(reply);

            if (result.artists.isEmpty()) {
                addAlbums(result.albums, count);
                return;
            }
            MultipleNetworkReplies* multipleArtists = new MultipleNetworkReplies();
            for (auto artist: result.artists) {
                multipleArtists->addReply(m_api->getArtist(artist.m_id));
            }
            QObject::connect(multipleArtists, &MultipleNetworkReplies::finished, [this, result, count, multipleArtists]() {
                AlbumList albums;
                for (auto r: multipleArtists->finishedReplies()) {
                    for (auto album: m_api->parseArtistReply(r).m_albums) {
                        if (hasAlbum(album.m_id, m_albums) || hasAlbum(album.m_id, result.albums)) continue;
                        albums.append(album);
                    }
                }
                addAlbums(result.albums + albums, count);
            });
        });
    }

}

void AlbumListModel::reload()
{
    int count = std::max(rowCount(QModelIndex()), FETCH_MORE_COUNT);
    clear();
    fetchMoreCount(count);
}

void AlbumListModel::clear()
{
    beginRemoveRows(QModelIndex(), 0, rowCount(QModelIndex()) - 1);
    m_albums.clear();
    m_allAlbumsFetched = false;
    endRemoveRows();
}

void AlbumListModel::addAlbums(const AlbumList& albums, int requestedCount)
{
    beginInsertRows(QModelIndex(), m_albums.size(), m_albums.size() + albums.size() - 1);
    m_albums += albums;
    m_allAlbumsFetched = albums.size() < requestedCount;
    m_fetchPending = false;
    endInsertRows();
}

bool AlbumListModel::hasAlbum(const QString &albumId, const AlbumList& albums) const
{
    for (auto album: albums) {
        if (album.m_id == albumId) return true;
    }
    return false;
}
