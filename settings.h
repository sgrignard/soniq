#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDir>
#include <QObject>
#include <QPointer>
#include <QUrl>

class QSettings;

class Settings: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QDir applicationDir READ applicationDir WRITE setApplicationDir NOTIFY applicationDirChanged)
    Q_PROPERTY(QUrl subsonicUrl READ subsonicUrl WRITE setSubsonicUrl NOTIFY subsonicUrlChanged)
    Q_PROPERTY(QString subsonicUser READ subsonicUser WRITE setSubsonicUser NOTIFY subsonicUserChanged)
    Q_PROPERTY(QString subsonicPassword READ subsonicPassword WRITE setSubsonicPassword NOTIFY subsonicPasswordChanged)
    Q_PROPERTY(int maximumCacheSize READ maximumCacheSize WRITE setMaximumCacheSize NOTIFY maximumCacheSizeChanged)
    Q_PROPERTY(int maximumSongCacheSize READ maximumSongCacheSize WRITE setMaximumSongCacheSize NOTIFY maximumSongCacheSizeChanged)
    Q_PROPERTY(QString subsonicStreamFormat READ subsonicStreamFormat WRITE setSubsonicStreamFormat NOTIFY subsonicStreamFormatChanged)

public:

    explicit Settings(QObject* parent = nullptr);

    static Settings* create(QSettings& qsettings);

    bool write(QSettings& qsettings) const;

    QDir applicationDir() const;

    QUrl subsonicUrl() const;

    QString subsonicUser() const;

    QString subsonicPassword() const;

    int maximumCacheSize() const;

    int maximumSongCacheSize() const;

    QString subsonicStreamFormat() const;

    QByteArray playlistData() const;

    static const Settings& instance();

    static void setInstance(Settings* settings);

public slots:

    void setApplicationDir(const QDir& dir);

    void setSubsonicUrl(const QUrl& url);

    void setSubsonicUser(const QString& user);

    void setSubsonicPassword(const QString& password);

    void setMaximumCacheSize(int size);

    void setMaximumSongCacheSize(int size);

    void setSubsonicStreamFormat(const QString& format);

    void setPlaylistData(const QByteArray& playlistData);

signals:

    void applicationDirChanged(const QDir& dir);

    void subsonicUrlChanged(const QUrl& url);

    void subsonicUserChanged(const QString& user);

    void subsonicPasswordChanged(const QString& password);

    void maximumCacheSizeChanged(int size);

    void maximumSongCacheSizeChanged(int size);

    void subsonicStreamFormatChanged(const QString& format);

private:


    QDir m_applicationDir;
    QUrl m_subsonicUrl;
    QString m_subsonicUser;
    QString m_subsonicPassword;
    int m_maximumCacheSize{10*1024*1024};
    int m_maximumSongCacheSize{10*1024*1024};
    QByteArray m_playlistData;
    QString m_subsonicStreamFormat;
    static QPointer<Settings> s_instance;
};

#endif // SETTINGS_H
