#ifndef APPLICATION_H
#define APPLICATION_H

#include <QGuiApplication>

#include "settings.h"

class Application : public QGuiApplication
{
    Q_OBJECT
    Q_PROPERTY(const Settings* settings READ settings)
public:
    Application(int& argc, char** argv);

    void loadSettings();

    Q_INVOKABLE const Settings* settings() const;

public slots:

    void updateSettings(const QVariant& settings);

signals:

    void settingsChanged(const Settings& settings);

private:

    Settings m_settings;
};

#endif // APPLICATION_H
