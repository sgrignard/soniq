import QtQuick 2.0

ShaderEffect {
    property alias source: image.source
    property color color
    property variant _src: Image { id: image }

    vertexShader: "
        uniform highp mat4 qt_Matrix;
        attribute highp vec4 qt_Vertex;
        attribute highp vec2 qt_MultiTexCoord0;
        varying highp vec2 coord;

        void main() {
            coord = qt_MultiTexCoord0;
            gl_Position = qt_Matrix * qt_Vertex;
        }
    "

    fragmentShader: "
        varying highp vec2 coord;
        uniform bool enabled;
        uniform sampler2D _src;
        uniform vec4 color;

        void main() {
            lowp vec4 sourceColor = texture2D(_src, coord);
            if ( sourceColor.a < 0.01) discard;
            if (enabled) {
                gl_FragColor = color;
            } else {
                gl_FragColor = sourceColor;
            }
        }
    "
}
