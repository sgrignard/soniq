import QtQuick 2.0
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15

Dialog {
    width: 800
    height: 600
    title: qsTr("Settings")
    readonly property int minimumFieldWidth: 200
    footer: DialogButtonBox {
        Button {
            text: qsTr("Ok")
            enabled: (
                subsonicUrl.acceptableInput &&
                subsonicUser.acceptableInput &&
                subsonicPassword.acceptableInput &&
                subsonicStreamFormat.acceptableInput &&
                maximumCacheSize.acceptableInput
            )
            DialogButtonBox.buttonRole: DialogButtonBox.AcceptRole
        }
        Button {
            text: qsTr("Cancel")
            DialogButtonBox.buttonRole: DialogButtonBox.RejectRole
        }
    }

    onAccepted: {
        settings.subsonicUrl = subsonicUrl.text
        settings.subsonicUser = subsonicUser.text
        settings.subsonicPassword = subsonicPassword.text
        settings.maximumCacheSize = 1e6*Number(maximumCacheSize.text)
        settings.subsonicStreamFormat = subsonicStreamFormat.text
    }
    onRejected: {}

    property var showAction: Action {
        id: showSettings
        text: qsTr("Settings…")
        icon.source : "qrc:///images/cogs.svg"
        onTriggered: settingsWindow.open()
    }

    GroupBox {
        title: qsTr("Network")
        width: parent.width
        GridLayout {
            columns: 2
            anchors.fill: parent

            Text { text: qsTr("Subsonic server URL:"); color: palette.text }
            ValidateTextField {
                id: subsonicUrl
                placeholderText: qsTr("https://your-subsonic.server.tld")
                text: settings.subsonicUrl
                validator: RegExpValidator { regExp: /http(s?)\:\/\/.+/}
                Layout.fillWidth: true
            }

            Text { text: qsTr("Subsonic user:"); color: palette.text }
            TextField {
                id: subsonicUser
                placeholderText: qsTr("user")
                text: settings.subsonicUser
                Layout.fillWidth: true
            }

            Text { text: qsTr("Subsonic password:"); color: palette.text }
            TextField {
                id: subsonicPassword
                text: settings.subsonicPassword
                echoMode: TextInput.Password
                Layout.fillWidth: true
            }

            Text { text: qsTr("Subsonic stream format:"); color: palette.text }
            TextField {
                id: subsonicStreamFormat
                placeholderText: "raw"
                text: settings.subsonicStreamFormat
                Layout.fillWidth: true
            }

            Text { text: qsTr("Maximum cache size (0 to disable):"); color: palette.text }
            ValidateTextField {
                id: maximumCacheSize
                text: Math.floor(settings.maximumCacheSize/1e6)
                validator: IntValidator{ bottom: 0 }
                Layout.fillWidth: true
            }
        }
    }
}
