# Soniq: a (very) simple Subsonic player written with Qml

## Compilation instructions

Install Qt with its development headers.
On Debian:
```shell
sudo apt install qt5base-dev qtquickcontrols2-5-dev
```

Then configure and build

```shell
cmake .
make -j4
```

## Roadmap

- [x] load albums until they fill the view
- [x] fetch more when scrolling
- [x] cache albums (at least their pictures)
- [x] search albums
- [x] improve buttons (play/pause/stop/erase playlist)
- [x] display the current song and its infos
- [x] preferences dialog (server URL, credentials)
- [x] play shortcut on albums
- [x] restyle playlist
- [x] clear playlist button
- [x] random, repeat playlist modes
- [x] MPRIS interface
- [x] error handling (network requests/…)
- [x] auto-save playlist
- [ ] lyrics through lyrics.ovh?
