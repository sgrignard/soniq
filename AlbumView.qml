import QtQuick 2.12
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Frame {
    id: albumView
    background: BackgroundRectangle {}
    property int albumSize: 200
    property var onAddAlbumClicked: (album) => {}
    property var onPlayAlbumClicked: (album) => {}
    property alias model: gridView.model
    ColumnLayout {
        anchors.fill: parent

        TextField {
            id: searchQuery
            placeholderText: qsTr("Search…")
            onAccepted: albumView.model.searchQuery = text
            Layout.fillWidth: true

            Shortcut {
                sequence: "Ctrl+F"
                onActivated: searchQuery.forceActiveFocus()
            }
            KeyNavigation.tab: gridView
        }
        GridView {
            id: gridView
            clip: true
            cellHeight: albumSize
            cellWidth: albumSize
            delegate: AlbumDelegate {
                size: albumSize
                onClicked: gridView.currentIndex = index
                onAddClicked: (album) => { albumView.onAddAlbumClicked(album) }
                onPlayClicked: (album) => { albumView.onPlayAlbumClicked(album) }
            }
            highlight: HighLightRectangle {}
            Layout.fillHeight: true
            Layout.fillWidth: true
            KeyNavigation.backtab: searchQuery
        }
    }
}
