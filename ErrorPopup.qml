import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Popup {
    id: popup
    property string message
    property string details
    closePolicy: Popup.CloseOnReleaseOutside|Popup.CloseOnEscape
    background: BackgroundRectangle {
        color: palette.light
        border.color: "orange"
        border.width: 1
        radius: 5
    }
    RowLayout {
        anchors.fill: parent
        Image {
            source: "qrc:///images/warning.svg"
            Layout.preferredWidth: soniqStyle.iconSize
            Layout.preferredHeight: soniqStyle.iconSize
        }

        Text {
            id: errorMessage
            color: palette.text
            text: details ? `${message} (${details})` : message
        }

        IconButton {
            onClicked: popup.close()
            size: soniqStyle.smallIconSize
            icon.source: "qrc:///images/close.svg"
            text: qsTr("Close popup")
        }
    }
}
