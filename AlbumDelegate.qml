import QtQuick 2.0
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

MouseArea {
    id: albumDelegate
    required property var album
    required property int index
    required property int size
    property var onPlayClicked: (album) => {}
    property var onAddClicked: (album) => {}
    width: size
    height: size
    hoverEnabled: true
    ColumnLayout {
        anchors.fill: parent
        Image {
            id: coverArt
            property real coverSize: 0.8*albumDelegate.size
            source: api.coverArtUrl(album.coverArt, coverSize)
            Layout.alignment: Qt.AlignHCenter
            Layout.preferredWidth: coverSize
            Layout.preferredHeight: coverSize

            RowLayout {
                visible: albumDelegate.containsMouse
                IconButton {
                    background: Rectangle { color: palette.window }
                    size: soniqStyle.smallIconSize
                    icon.source: "qrc:///images/play.svg"
                    text: qsTr("Play album")
                    onClicked: () => { albumDelegate.onPlayClicked(album) }
                }
                IconButton {
                    background: Rectangle { color: palette.window }
                    size: soniqStyle.smallIconSize
                    icon.source: "qrc:///images/plus.svg"
                    text: qsTr("Add album to playlist")
                    onClicked: () => { albumDelegate.onAddClicked(album) }
                }
                anchors.top: parent.top
                anchors.topMargin: 5
                anchors.right: parent.right
                anchors.rightMargin: 5
            }
        }
        Text {
            id: name
            // Needed as subsonic search returns the name of an album as "title"…
            text: album.name || album.title
            color: palette.text
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignHCenter
            maximumLineCount: 2
            elide: Text.ElideRight
            Layout.alignment: Qt.AlignHCenter
            Layout.maximumWidth: albumDelegate.size
        }
    }

    Keys.onPressed: {
        if (event.key === Qt.Key_Return && event.modifiers === Qt.ShiftModifier)
            albumDelegate.onPlayClicked(album)
        else if (event.key === Qt.Key_Return)
            albumDelegate.onAddClicked(album)
    }
}
