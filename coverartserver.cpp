#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QTcpServer>
#include <QTcpSocket>

#include "subsonicapi.h"

#include "coverartserver.h"

#define CRLF "\r\n"

CoverArtServer::CoverArtServer(QObject *parent)
    : QObject(parent)
    , m_server(new QTcpServer(this))
{
    m_server->listen(QHostAddress::LocalHost);
    QObject::connect(m_server, &QTcpServer::newConnection, [this]() {
        QTcpSocket* connection = m_server->nextPendingConnection();
        QObject::connect(connection, &QTcpSocket::readyRead, this, &CoverArtServer::handleRequest);
    });
}

void CoverArtServer::setApi(SubsonicApi *api)
{
    m_api = api;
}

void CoverArtServer::setNetworkAccessManager(QNetworkAccessManager *manager)
{
    m_manager = manager;
}

QString CoverArtServer::baseUrl() const
{
    auto u = QString("http://%1:%2").arg(m_server->serverAddress().toString()).arg(m_server->serverPort());
    qDebug() << "base url" << u;
    return u;
}

void CoverArtServer::handleRequest()
{
    auto socket = qobject_cast<QTcpSocket*>(sender());
    auto data = m_requestData.value(socket, nullptr);
    if (!data) {
        data = new RequestData();
        m_requestData[socket] = data;
        QObject::connect(socket, &QTcpSocket::bytesWritten, [this, socket, data](qint64 bytes) {
            data->writtenLength += bytes;
            if (data->writtenLength >= data->responseLength) {
                socket->deleteLater();
                m_requestData.remove(socket);
                delete data;
            }
        });
    }
    data->request += socket->readAll();
    // did not get the full request line
    if (!data->request.contains(CRLF)) return;

    QList<QByteArray> params = data->request.split(' ');
    if (params.size()<2) {
        socket->write(QByteArray("HTTP/1.0 400 Bad Request"));
        socket->deleteLater();
        return;
    }
    auto url = m_api->coverArtUrl(params.at(1).mid(1), 200);
    auto reply = m_manager->get(QNetworkRequest(url));
    connect(reply, &QNetworkReply::finished, [reply, socket, data] {
        reply->deleteLater();
        data->responseLength += socket->write("HTTP/1.0 200 OK");
        data->responseLength += socket->write(CRLF);
        for (auto header: reply->rawHeaderPairs()) {
            data->responseLength += socket->write(header.first);
            data->responseLength += socket->write(": ");
            data->responseLength += socket->write(header.second);
            data->responseLength += socket->write(CRLF);
        }
        data->responseLength += socket->write(CRLF);
        data->responseLength += socket->write(reply->readAll());
    });
}
