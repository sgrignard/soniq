#ifndef ALBUMLISTMODEL_H
#define ALBUMLISTMODEL_H

#include <QAbstractListModel>

#include "album.h"
#include "subsonicapi.h"

class AlbumListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QString searchQuery READ searchQuery WRITE setSearchQuery NOTIFY searchQueryChanged)
public:

    static const int AlbumRole;

    AlbumListModel(SubsonicApi* api);

    virtual int rowCount(const QModelIndex&) const;

    virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;

    virtual bool canFetchMore(const QModelIndex&) const;

    Q_INVOKABLE virtual void fetchMore(const QModelIndex& = QModelIndex());

    Q_INVOKABLE void fetchMoreCount(int count);

    virtual QHash<int, QByteArray> roleNames() const;

    QString searchQuery() const;

public slots:

    void reload();

    void clear();

    void setSearchQuery(const QString& query);

signals:

    void searchQueryChanged(const QString& query);

private:

    void addAlbums(const AlbumList& albums, int requestedCount);

    bool hasAlbum(const QString& albumId, const AlbumList& albums) const;

    SubsonicApi* m_api;
    AlbumList m_albums;
    bool m_allAlbumsFetched;
    bool m_fetchPending;
    QString m_searchQuery;
};

#endif // ALBUMLISTMODEL_H
