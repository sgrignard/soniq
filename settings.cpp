#include <QDir>
#include <QScopedPointer>
#include <QSettings>

#include "settings.h"

static const QDir DEFAULT_APPLICATION_DIR = (QDir::homePath() + "/.soniq");
static const int DEFAULT_NETWORK_MAXIMUM_CACHE_SIZE = 100*1024*1024;

QPointer<Settings> Settings::s_instance;

Settings::Settings(QObject* parent)
    : QObject(parent)
{
}

Settings* Settings::create(QSettings &qsettings)
{
    QScopedPointer<Settings> settings(new Settings());
    QString path = qsettings.value("applicationDir", DEFAULT_APPLICATION_DIR.absolutePath()).toString();
    if (!QDir(path).exists() && !QDir().mkdir(path)) return nullptr;
    settings->setApplicationDir(QDir(path));

    bool rok;
    qsettings.beginGroup("network");
    settings->setSubsonicUrl(qsettings.value("subsonicUrl").toString());
    settings->setSubsonicUser(qsettings.value("subsonicUser").toString());
    settings->setSubsonicPassword(qsettings.value("subsonicPassword").toString());
    settings->setMaximumCacheSize(qsettings.value("maximumCacheSize", DEFAULT_NETWORK_MAXIMUM_CACHE_SIZE).toInt(&rok));
    settings->setSubsonicStreamFormat(qsettings.value("subsonicStreamFormat", "raw").toString());
    qsettings.endGroup();
    if (!rok) return nullptr;


    settings->setPlaylistData(qsettings.value("playlistData", QByteArray()).toByteArray());
    if (!rok) return nullptr;
    return settings.take();
}

bool Settings::write(QSettings &qsettings) const
{
    qsettings.setValue("applicationDir", m_applicationDir.absolutePath());
    qsettings.beginGroup("network");
    qsettings.setValue("subsonicUrl", m_subsonicUrl.toString());
    qsettings.setValue("subsonicUser", m_subsonicUser);
    qsettings.setValue("subsonicPassword", m_subsonicPassword);
    qsettings.setValue("maximumCacheSize", m_maximumCacheSize);
    qsettings.setValue("subsonicStreamFormat", m_subsonicStreamFormat);
    qsettings.endGroup();
    qsettings.setValue("playlistData", m_playlistData);

    return true;
}

QDir Settings::applicationDir() const
{
    return m_applicationDir;
}

QUrl Settings::subsonicUrl() const
{
    return m_subsonicUrl;
}

QString Settings::subsonicUser() const
{
    return m_subsonicUser;
}

QString Settings::subsonicPassword() const
{
    return m_subsonicPassword;
}

int Settings::maximumCacheSize() const
{
    return m_maximumCacheSize;
}

int Settings::maximumSongCacheSize() const
{
    return m_maximumSongCacheSize;
}

QString Settings::subsonicStreamFormat() const
{
    return m_subsonicStreamFormat;
}

QByteArray Settings::playlistData() const
{
    return m_playlistData;
}

const Settings &Settings::instance()
{
    assert (!s_instance.isNull());
    return *s_instance.data();
}

void Settings::setInstance(Settings *settings)
{
    assert (s_instance.isNull());
    s_instance = settings;
}

void Settings::setApplicationDir(const QDir& dir)
{
    if (dir == m_applicationDir) return;
    m_applicationDir = dir;
    emit applicationDirChanged(dir);
}

void Settings::setSubsonicUrl(const QUrl& url)
{
    if (url == m_subsonicUrl) return;
    m_subsonicUrl = url;
    emit subsonicUrlChanged(m_subsonicUrl);
}

void Settings::setSubsonicUser(const QString& user)
{
    if (user == m_subsonicUser) return;
    m_subsonicUser = user;
    emit subsonicUserChanged(user);
}

void Settings::setSubsonicPassword(const QString& password)
{
    if (password == m_subsonicPassword) return;
    m_subsonicPassword = password;
    emit subsonicPasswordChanged(password);
}

void Settings::setMaximumCacheSize(int size)
{
    if (size == m_maximumCacheSize) return;
    m_maximumCacheSize = size;
    emit maximumCacheSizeChanged(size);
}

void Settings::setMaximumSongCacheSize(int size)
{
    if (size == m_maximumSongCacheSize) return;
    m_maximumSongCacheSize = size;
    emit maximumSongCacheSizeChanged(size);
}

void Settings::setSubsonicStreamFormat(const QString &format)
{
    if (format == m_subsonicStreamFormat) return;
    m_subsonicStreamFormat = format;
    emit subsonicStreamFormatChanged(format);
}

void Settings::setPlaylistData(const QByteArray &playlistData)
{
    m_playlistData = playlistData;
}
