import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import "util.js" as Util

Item {
    required property var selectionModel
    property var onSongDoubleClicked: () => {}
    property alias model: view.model
    Layout.fillHeight: true
    Layout.fillWidth: true
    HorizontalHeaderView {
        id: songProperties
        syncView: view
        anchors.left: view.left
    }
    VerticalHeaderView {
        id: songNumbers
        syncView: view
        anchors.top: view.top
    }
    TableView {
        id: view
        interactive: false
        anchors.fill: parent
        anchors.topMargin: songProperties.height
        anchors.leftMargin: songNumbers.width
        clip: true
        columnWidthProvider: function (column) {
            // Hide the id column
            if (column === 0) return 0
            //const rowWidth = Util.getMaxRowWidth(view)
            return Util.getMaxColumnItemWidth(column, view)
            //console.log(rowWidth, width)
            //return rowWidth > width ? columnWidth : width*columnWidth/rowWidth
        }

        delegate: PlaylistDelegate {
            isCurrentIndex: selectionModel.currentIndex.row === row
            onClicked: () => selectionModel.selectRow(row)
            onDoubleClicked: (song) => {
                selectionModel.setCurrentRow(row)
                onSongDoubleClicked(song, row)
            }

            color: {
                const modelIndex = view.model.index(row, column)
                const isSelected = selectionModel.selectedIndexes.some(index => index === modelIndex)
                if (isSelected) return palette.highlight
                if (row%2) return palette.base
                return soniqStyle.alternateBase
            }
        }
    }
}
