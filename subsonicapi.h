#ifndef SUBSONICAPI_H
#define SUBSONICAPI_H

#include <QList>
#include <QNetworkAccessManager>
#include <QObject>
#include <QUrl>

#include "album.h"

class QDomDocument;

class SubsonicApiError {
    Q_GADGET
    Q_PROPERTY(int httpStatus MEMBER httpStatus)
    Q_PROPERTY(QString message MEMBER message)
    Q_PROPERTY(int errorCode MEMBER code)
    Q_PROPERTY(QString details MEMBER details)
public:

    enum ErrorCode {
        // Official subsonic error codes
        Generic = 0,
        MissingRequiredParameter = 10,
        IncompatibleClientRESTVersion = 20,
        IncompatibleServerRESTVersion = 30,
        InvalidCredentials = 40,
        Forbidden = 50,
        UnsupportedTokenForLDAPUsers = 51,
        TrialExpired = 60,
        DataNotFound = 70,

        // Soniq error codes
        MalformedXMLDocument = 1000,
        NotSubsonicResponse = 1001,
        HttpStatusNotOk = 1002
    };
    Q_ENUM(ErrorCode);

    int httpStatus;
    QString message;
    int code;
    QString details;
};

struct SearchResult {
    AlbumList albums;
    ArtistList artists;
};


class MultipleNetworkReplies: public QObject
{
    Q_OBJECT
public:
    MultipleNetworkReplies();

    void addReply(QNetworkReply* reply);

    QList<QNetworkReply*> finishedReplies() const;

signals:

    void finished();

private slots:

    void replyFinished();

private:
    QList<QPointer<QNetworkReply> > m_replies;
};

class SubsonicApi : public QObject
{
    Q_OBJECT
public:
    typedef enum{
        RANDOM,
        NEWEST,
        ALPHABETICAL_BY_NAME
    } AlbumListType;

    explicit SubsonicApi(QNetworkAccessManager* parent);

    QUrl url() const;

    QNetworkAccessManager* manager() const;

    Q_INVOKABLE AlbumList parseAlbumListReply(QNetworkReply* reply);

    Q_INVOKABLE Album parseAlbumReply(QNetworkReply* reply);

    Q_INVOKABLE Artist parseArtistReply(QNetworkReply* reply);

    Q_INVOKABLE SearchResult parseSearchReply(QNetworkReply* reply);

    Q_INVOKABLE QString coverArtUrl(const QString& coverArtId, int size = 100) const;

    Q_INVOKABLE QString streamUrl(const QString& fileId, const QString& format = "raw") const;

    Q_INVOKABLE QNetworkReply* getAlbumList2(
            AlbumListType type,
            uint32_t size = 10,
            uint32_t offset = 0,
            uint32_t fromYear = 0,
            uint32_t toYear = 0,
            const QString& genre = QString()) const;

    Q_INVOKABLE QNetworkReply *getSearch3(
            const QString& query,
            int artistCount = 10,
            int artistOffset = 0,
            int albumCount = 10,
            int albumOffset = 0,
            int songCount = 10,
            int songOffset = 0) const;

    Q_INVOKABLE QNetworkReply* getAlbum(const QString& albumId) const;

    Q_INVOKABLE QNetworkReply* getArtist(const QString& artistId) const;

    Q_INVOKABLE QVariant album(const QString& albumId) const;

    QString songId(const QUrl& streamUrl) const;

public slots:

    void setUrl(const QUrl& url);

    void setCredentials(const QString& user, const QString& password);

signals:

    void urlChanged(const QUrl& url);

    void credentialsChanged(const QString& user, const QString& password);

    void errorOccurred(const SubsonicApiError& error) const;

private:

    QUrl getUrl(const QString& path, const QUrlQuery& query) const;

    QNetworkReply* get(const QString& path, const QUrlQuery& query) const;

    QDomDocument getDocument(QNetworkReply* reply) const;

    QNetworkAccessManager* m_manager;
    QUrl m_url;
    QString m_user;
    QString m_password;
};

#endif // SUBSONICAPI_H
