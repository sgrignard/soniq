<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>AlbumDelegate</name>
    <message>
        <location filename="AlbumDelegate.qml" line="31"/>
        <source>Play album</source>
        <translation>Play album</translation>
    </message>
    <message>
        <location filename="AlbumDelegate.qml" line="38"/>
        <source>Add album to playlist</source>
        <translation>Add album to playlist</translation>
    </message>
</context>
<context>
    <name>AlbumView</name>
    <message>
        <location filename="AlbumView.qml" line="17"/>
        <source>Search…</source>
        <translation>Search…</translation>
    </message>
</context>
<context>
    <name>ErrorPopup</name>
    <message>
        <location filename="ErrorPopup.qml" line="34"/>
        <source>Close popup</source>
        <translation>Close popup</translation>
    </message>
</context>
<context>
    <name>PlayerControls</name>
    <message>
        <location filename="PlayerControls.qml" line="15"/>
        <source>Previous</source>
        <translation>Previous</translation>
    </message>
    <message>
        <location filename="PlayerControls.qml" line="26"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="PlayerControls.qml" line="26"/>
        <source>Play</source>
        <translation>Play</translation>
    </message>
    <message>
        <location filename="PlayerControls.qml" line="39"/>
        <source>Next</source>
        <translation>Next</translation>
    </message>
</context>
<context>
    <name>PlaylistControls</name>
    <message>
        <location filename="PlaylistControls.qml" line="17"/>
        <source>Sequential playback</source>
        <translation>Sequential playback</translation>
    </message>
    <message>
        <location filename="PlaylistControls.qml" line="22"/>
        <source>Random playback</source>
        <translation>Random playback</translation>
    </message>
    <message>
        <location filename="PlaylistControls.qml" line="27"/>
        <source>Repeat</source>
        <translation>Repeat</translation>
    </message>
    <message>
        <location filename="PlaylistControls.qml" line="44"/>
        <source>Clear playlist</source>
        <translation>Clear playlist</translation>
    </message>
</context>
<context>
    <name>PlaylistModel</name>
    <message>
        <location filename="playlistmodel.cpp" line="11"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <location filename="playlistmodel.cpp" line="12"/>
        <source>Title</source>
        <translation>Title</translation>
    </message>
    <message>
        <location filename="playlistmodel.cpp" line="13"/>
        <source>Album</source>
        <translation>Album</translation>
    </message>
    <message>
        <location filename="playlistmodel.cpp" line="14"/>
        <source>Artist</source>
        <translation>Artist</translation>
    </message>
    <message>
        <location filename="playlistmodel.cpp" line="15"/>
        <source>Duration</source>
        <translation>Duration</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="Settings.qml" line="9"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="Settings.qml" line="13"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="Settings.qml" line="24"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="Settings.qml" line="40"/>
        <source>Settings…</source>
        <translation>Settings…</translation>
    </message>
    <message>
        <location filename="Settings.qml" line="46"/>
        <source>Network</source>
        <translation>Network</translation>
    </message>
    <message>
        <location filename="Settings.qml" line="52"/>
        <source>Subsonic server URL:</source>
        <translation>Subsonic server URL:</translation>
    </message>
    <message>
        <location filename="Settings.qml" line="55"/>
        <source>https://your-subsonic.server.tld</source>
        <translation>https://your-subsonic.server.tld</translation>
    </message>
    <message>
        <location filename="Settings.qml" line="61"/>
        <source>Subsonic user:</source>
        <translation>Subsonic user:</translation>
    </message>
    <message>
        <location filename="Settings.qml" line="64"/>
        <source>user</source>
        <translation>user</translation>
    </message>
    <message>
        <location filename="Settings.qml" line="69"/>
        <source>Subsonic password:</source>
        <translation>Subsonic password:</translation>
    </message>
    <message>
        <location filename="Settings.qml" line="77"/>
        <source>Subsonic stream format:</source>
        <translation>Subsonic stream format:</translation>
    </message>
    <message>
        <location filename="Settings.qml" line="85"/>
        <source>Maximum cache size (0 to disable):</source>
        <translation>Maximum cache size (0 to disable):</translation>
    </message>
</context>
<context>
    <name>SubsonicApi</name>
    <message>
        <location filename="subsonicapi.cpp" line="219"/>
        <location filename="subsonicapi.cpp" line="234"/>
        <source>Got an error from the server</source>
        <translation>Got an error from the server</translation>
    </message>
    <message>
        <location filename="subsonicapi.cpp" line="247"/>
        <source>Malformed XML document at line %1, column %2</source>
        <translation>Malformed XML document at line %1, column %2</translation>
    </message>
    <message>
        <location filename="subsonicapi.cpp" line="258"/>
        <source>Not a subsonic response</source>
        <translation>Not a subsonic response</translation>
    </message>
    <message>
        <location filename="subsonicapi.cpp" line="269"/>
        <source>Subsonic API error</source>
        <translation>Subsonic API error</translation>
    </message>
</context>
<context>
    <name>VolumeControl</name>
    <message>
        <location filename="VolumeControl.qml" line="29"/>
        <source>Muted</source>
        <translation>Muted</translation>
    </message>
    <message>
        <location filename="VolumeControl.qml" line="29"/>
        <source>Volume at %1%</source>
        <translation>Volume at %1%</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="main.qml" line="15"/>
        <source>Soniq</source>
        <translation>Soniq</translation>
    </message>
</context>
</TS>
