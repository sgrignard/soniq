#include <QDBusConnection>
#include <QDBusMessage>
#include <QGuiApplication>
#include <QWindow>

#include "playlistcontroller.h"
#include "playlistselectionmodel.h"
#include "playlistmodel.h"

#include "mpris.h"

const QString PlayerInterface = "org.mpris.MediaPlayer2.Player";

const QMap<QMediaPlayer::State, QString> PLAYER_STATES = {
    {QMediaPlayer::PausedState,  "Paused" },
    {QMediaPlayer::PlayingState, "Playing"},
    {QMediaPlayer::StoppedState, "Stopped"},
};

void MediaPlayer2::notifyPropertyChanged(const QString& interface, const QString& propertyName)
{
    QDBusMessage signal = QDBusMessage::createSignal(
        m_path,
        "org.freedesktop.DBus.Properties",
        "PropertiesChanged");
    signal << interface;
    QVariantMap changedProps;
    changedProps.insert(propertyName, property(propertyName.toLatin1()));
    signal << changedProps;
    signal << QStringList();
    QDBusConnection::sessionBus().send(signal);
}

MediaPlayer2::MediaPlayer2(const QString& path, PlaylistController* controller)
    : QObject(controller)
    , m_path(path)
    , m_controller(controller)
{
    qRegisterMetaType<StringVariantMap>("StringVariantMap");
    QObject::connect(controller->player(), &QMediaPlayer::stateChanged, [this](auto state) {
        auto stateString = PLAYER_STATES.value(state);
        emit playbackStatusChanged(stateString);
        notifyPropertyChanged(PlayerInterface, "PlaybackStatus");
    });
    QObject::connect(controller, &PlaylistController::playlistPositionChanged, [this]() {
       notifyPropertyChanged(PlayerInterface, "Metadata");
    });
    QObject::connect(controller->player(), &QMediaPlayer::positionChanged, [this]() {
        notifyPropertyChanged(PlayerInterface, "Position");
    });
}

void MediaPlayer2::setCoverArtBaseUrl(const QString &baseUrl)
{
    m_coverArtBaseUrl = baseUrl;
}

QString MediaPlayer2::identity() const
{
    return QCoreApplication::instance()->applicationName();
}

bool MediaPlayer2::canQuit() const
{
    auto app = QCoreApplication::instance();
    return !(app->startingUp() || app->closingDown());
}

bool MediaPlayer2::canRaise() const
{
    auto app = qobject_cast<QGuiApplication*>(QCoreApplication::instance());
    return !app->topLevelWindows().isEmpty();
}

bool MediaPlayer2::canSetFullscreen() const
{
    auto app = qobject_cast<QGuiApplication*>(QCoreApplication::instance());
    return !app->topLevelWindows().isEmpty();
}

bool MediaPlayer2::fullscreen() const
{
    QGuiApplication* app = qobject_cast<QGuiApplication*>(QCoreApplication::instance());
    for (auto window: app->topLevelWindows()) {
        if (!(window->windowState()&Qt::WindowFullScreen)) return false;
    }
    return true;
}

QString MediaPlayer2::desktopEntry() const
{
    return QString();
}

bool MediaPlayer2::hasTrackList() const
{
    return false;
}

QStringList MediaPlayer2::supportedMimeTypes() const
{
    return m_controller->player()->supportedMimeTypes();
}

QStringList MediaPlayer2::supportedUriSchemes() const
{
    return QStringList() << "https://" << "http://";
}

void MediaPlayer2::Quit()
{
    QCoreApplication::instance()->quit();
}

void MediaPlayer2::Raise()
{
    QGuiApplication* app = qobject_cast<QGuiApplication*>(QCoreApplication::instance());
    for (auto window: app->topLevelWindows()) {
        window->raise();
    }
}

void MediaPlayer2::setFullscreen(bool fullscreen)
{
    QGuiApplication* app = qobject_cast<QGuiApplication*>(QCoreApplication::instance());
    for (auto window: app->topLevelWindows()) {
        if (fullscreen) {
            window->showFullScreen();
        } else {
            window->showNormal();
        }
    }
}

QString MediaPlayer2::loopStatus() const
{
    return "Playlist";
}

QString MediaPlayer2::playbackStatus() const
{
    auto status = PLAYER_STATES.value(m_controller->player()->state());
    return status;
}

double MediaPlayer2::rate() const
{
    return m_controller->player()->playbackRate();
}

bool MediaPlayer2::shuffle() const
{
    return m_controller->playbackMode()==PlaylistController::PlaybackMode::Random;
}

bool MediaPlayer2::canControl() const
{
    return true;
}

bool MediaPlayer2::canGoNext() const
{
    return m_controller->playlistPosition() +1 < m_controller->model()->itemCount();
}

bool MediaPlayer2::canGoPrevious() const
{
    return m_controller->playlistPosition() > 0;
}

bool MediaPlayer2::canPause() const
{
    return true;
}

bool MediaPlayer2::canPlay() const
{
    return true;
}

bool MediaPlayer2::canSeek() const
{
    return true;
}

double MediaPlayer2::maximumRate() const
{
    return 100.;
}

StringVariantMap MediaPlayer2::metadata() const
{
    auto song = m_controller->model()->song(m_controller->selectionModel()->currentIndex().row());
    auto metadata = StringVariantMap({
        {"mpris:trackid", m_path + QString("/%1").arg(song.m_id)},
        {"mpris:length", 1e6 * song.m_duration},
        {"mpris:artUrl", QString("%1/%2").arg(m_coverArtBaseUrl).arg(song.m_coverArtId)},
        {"xesam:title", song.m_title},
        {"xesam:album", song.m_album},
        {"xesam:artist", song.m_artist},
    });
    return metadata;
}

double MediaPlayer2::minimumRate() const
{
    return 0.1;
}

qlonglong MediaPlayer2::position() const
{
    return 1e3 * m_controller->player()->position();
}

double MediaPlayer2::volume() const
{
    return m_controller->player()->volume()/100.;
}

void MediaPlayer2::Next()
{
    m_controller->goNext();
}

void MediaPlayer2::OpenUri(const QString &)
{
}

void MediaPlayer2::Pause()
{
    m_controller->player()->pause();
}

void MediaPlayer2::Play()
{
    m_controller->player()->play();
}

void MediaPlayer2::PlayPause()
{
    if (QMediaPlayer::PlayingState == m_controller->player()->state()) {
        m_controller->player()->pause();
    } else {
        m_controller->player()->play();
    }
}

void MediaPlayer2::Previous()
{
    m_controller->goPrevious();
}

void MediaPlayer2::Seek(qlonglong Offset)
{
    qint64 newPos = m_controller->player()->position() + qint64(1e-3*Offset);
    m_controller->player()->setPosition(newPos);
}

void MediaPlayer2::SetPosition(const QDBusObjectPath &, qlonglong)
{
}

void MediaPlayer2::Stop()
{
    m_controller->player()->stop();
}

void MediaPlayer2::setLoopStatus(const QString &)
{

}

void MediaPlayer2::setRate(double rate)
{
    m_controller->player()->setPlaybackRate(rate);
}

void MediaPlayer2::setShuffle(bool shuffle)
{
    auto mode = shuffle ? PlaylistController::PlaybackMode::Random : PlaylistController::PlaybackMode::Sequential;
    m_controller->setPlaybackMode(mode);
}

void MediaPlayer2::setVolume(double value)
{
    m_controller->player()->setVolume(100*value);
}
