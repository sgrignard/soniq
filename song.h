#ifndef SONG_H
#define SONG_H

#include <QObject>

struct Song {
    Q_GADGET
    Q_PROPERTY(QString id MEMBER m_id)
    Q_PROPERTY(QString title MEMBER m_title)

    QString m_id;
    QString m_title;
};

#endif // SONG_H
