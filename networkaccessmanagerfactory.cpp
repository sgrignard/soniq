#include <QDir>
#include <QNetworkAccessManager>
#include <QNetworkDiskCache>
#include <QPointer>

#include "settings.h"

#include "networkaccessmanagerfactory.h"

NetworkAccessManagerFactory::NetworkAccessManagerFactory()
{
}

QNetworkAccessManager *NetworkAccessManagerFactory::create(QObject *parent)
{
    // TODO: error case
    auto applicationDir = Settings::instance().applicationDir();
    auto cacheDirName = "cache-" + (parent->objectName().isNull() ? "default" : parent->objectName());
    if (!applicationDir.exists(cacheDirName) && !applicationDir.mkdir(cacheDirName)) return nullptr;
    QNetworkDiskCache* cache = new QNetworkDiskCache();
    cache->setCacheDirectory(applicationDir.absoluteFilePath(cacheDirName));
    cache->setMaximumCacheSize(Settings::instance().maximumCacheSize());
    QNetworkAccessManager* manager = new QNetworkAccessManager(parent);
    manager->setCache(cache);
    return manager;
}
