import QtQml.Models 2.15
import QtQuick 2.0

Rectangle {
    property var onClicked: (mouse) => {}
    property var onDoubleClicked: (mouse) => {}
    required property var song
    required property var isCurrentIndex
    required property string display
    required property string propertyName
    required property int row
    required property int column

    implicitWidth: textDisplay.width + 10
    implicitHeight: textDisplay.height
    Text {
        id: textDisplay
        text: propertyName !== "duration" ? display : durationString(display)
        anchors.horizontalCenter: parent.horizontalCenter
        elide: Text.ElideRight
        font.bold: isCurrentIndex
        color: palette.text
        wrapMode: Text.Wrap
        HoverToolTip {
            text: textDisplay.text
        }

        function durationString(duration) {
            const minutes = (Math.trunc(duration / 60)).toString()
            const seconds = (duration % 60).toString().padStart(2, "0")
            return `${minutes}:${seconds}`
        }
    }
    MouseArea {
        anchors.fill: parent
        onClicked: parent.onClicked(mouse)
        onDoubleClicked: parent.onDoubleClicked(mouse)
    }
}
