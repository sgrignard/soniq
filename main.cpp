#include <QDBusConnection>
#include <QGuiApplication>
#include <QMediaPlayer>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickStyle>
#include <QSettings>
#include <QTranslator>

#include "albumlistmodel.h"
#include "coverartserver.h"
#include "mediaplayer2adaptor.h"
#include "mpris.h"
#include "playeradaptor.h"
#include "playlistcontroller.h"
#include "playlistmodel.h"
#include "playlistselectionmodel.h"
#include "networkaccessmanagerfactory.h"
#include "settings.h"
#include "subsonicapi.h"


int main(int argc, char *argv[])
{
    qmlRegisterUncreatableType<Song>("Soniq", 1, 0, "Song", "cannot");
    qRegisterMetaType<Song>();
    qmlRegisterUncreatableType<Album>("Soniq", 1, 0, "Album", "cannot");
    qRegisterMetaType<Album>();
    qmlRegisterUncreatableType<QMediaPlayer>("Soniq", 1, 0, "QMediaPlayer", "cannot");
    qmlRegisterUncreatableType<PlaylistController>("Soniq", 1, 0, "PlaylistController", "cannot");
    qmlRegisterUncreatableType<PlaylistModel>("Soniq", 1, 0, "PlaylistModel", "cannot");
    qmlRegisterUncreatableType<SubsonicApiError>("Soniq", 1, 0, "SubsonicApiError", "cannot");
    qRegisterMetaType<SubsonicApiError>();
    //qRegisterMetaType<QMediaPlayer>();

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);
    app.setObjectName("app");
    app.setApplicationName("Soniq");
    app.setOrganizationName("Soniq");
    app.setOrganizationDomain("soniq.org");
    QQuickStyle::setStyle("Fusion");

    QTranslator translator;
    qDebug() << QLocale().uiLanguages();
    if (translator.load(QLocale(), QLatin1String("soniq"), QLatin1String("_"), QLatin1String(":/translations")))
        QCoreApplication::installTranslator(&translator);

    QSettings qsettings;
    Settings* settings = Settings::create(qsettings);
    Settings::setInstance(settings);

    NetworkAccessManagerFactory networkManagerFactory;
    QNetworkAccessManager* manager = networkManagerFactory.create(&app);

    SubsonicApi* api = new SubsonicApi(manager);
    QObject::connect(settings, &Settings::subsonicUrlChanged, api, &SubsonicApi::setUrl);
    api->setUrl(settings->subsonicUrl());
    auto setCredentials = [api, settings]() {
        api->setCredentials(settings->subsonicUser(), settings->subsonicPassword());
    };
    QObject::connect(settings, &Settings::subsonicUserChanged, setCredentials);
    QObject::connect(settings, &Settings::subsonicPasswordChanged, setCredentials);
    setCredentials();

    AlbumListModel* albumModel = new AlbumListModel(api);
    PlaylistController* playlistController = new PlaylistController(&app);
    playlistController->setApi(api);
    playlistController->setNetworkAccessManager(manager);
    playlistController->setStreamFormat(settings->subsonicStreamFormat());
    QObject::connect(settings, &Settings::subsonicStreamFormatChanged, playlistController, &PlaylistController::setStreamFormat);
    if (!settings->playlistData().isEmpty()) {
        playlistController->load(settings->playlistData());
    }
    QObject::connect(&app, &QGuiApplication::aboutToQuit, [settings, &qsettings, playlistController]() {
        settings->setPlaylistData(playlistController->save());
        settings->write(qsettings);
    });

    // connect to D-Bus and register as an object
    QString path = "/org/mpris/MediaPlayer2";
    auto coverArtServer = new CoverArtServer(&app);
    coverArtServer->setApi(api);
    coverArtServer->setNetworkAccessManager(manager);
    auto mediaPlayer2Implementation = new MediaPlayer2(path, playlistController);
    mediaPlayer2Implementation->setCoverArtBaseUrl(coverArtServer->baseUrl());
    new MediaPlayer2Adaptor(mediaPlayer2Implementation);
    new PlayerAdaptor(mediaPlayer2Implementation);
    auto dbus = QDBusConnection::sessionBus();
    dbus.registerObject(path, mediaPlayer2Implementation);
    dbus.registerService("org.mpris.MediaPlayer2.soniq");


    QQmlApplicationEngine engine;
    engine.setNetworkAccessManagerFactory(&networkManagerFactory);
    engine.rootContext()->setContextProperty("app", &app);
    engine.rootContext()->setContextProperty("albumModel", albumModel);
    engine.rootContext()->setContextProperty("api", api);
    engine.rootContext()->setContextProperty("settings", settings);
    engine.rootContext()->setContextProperty("mediaPlayer", playlistController->player());
    engine.rootContext()->setContextProperty("playlistModel", playlistController->model());
    engine.rootContext()->setContextProperty("playlistSelectionModel", playlistController->selectionModel());
    engine.rootContext()->setContextProperty("playlistController", playlistController);
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);


    engine.load(url);
    return app.exec();
}
