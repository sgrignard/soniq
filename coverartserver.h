#ifndef COVERARTSERVER_H
#define COVERARTSERVER_H

#include <QMap>
#include <QObject>
#include <QPointer>

class QNetworkAccessManager;
class QTcpServer;
class QTcpSocket;
class SubsonicApi;

class CoverArtServer : public QObject
{
    Q_OBJECT
public:
    explicit CoverArtServer(QObject *parent = nullptr);

    void setApi(SubsonicApi* api);

    void setNetworkAccessManager(QNetworkAccessManager* manager);

    QString baseUrl() const;

private slots:

    void handleRequest();

private:

    struct RequestData {
      QByteArray request;
      int responseLength{0};
      int writtenLength{0};
    };

    QPointer<QTcpServer> m_server;
    QPointer<SubsonicApi> m_api;
    QPointer<QNetworkAccessManager> m_manager;
    QMap<QObject*, RequestData*> m_requestData;
};

#endif // COVERARTSERVER_H
