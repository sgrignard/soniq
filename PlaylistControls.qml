import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import Soniq 1.0

ToolBar {
    required property PlaylistController controller
    required property var model
    ButtonGroup {
        id: playbackModeGroup
    }
    RowLayout {
        Repeater {
            model: ListModel {
                ListElement {
                    buttonText: qsTr("Sequential playback")
                    iconSource: "qrc:///images/sequential.svg"
                    playbackMode: PlaylistController.Sequential
                }
                ListElement {
                    buttonText: qsTr("Random playback")
                    iconSource: "qrc:///images/random.svg"
                    playbackMode: PlaylistController.Random
                }
                ListElement {
                    buttonText: qsTr("Repeat")
                    iconSource: "qrc:///images/repeat.svg"
                    playbackMode: PlaylistController.Loop
                }
            }
            IconButton {
                text: buttonText
                icon.source: iconSource
                size: soniqStyle.smallIconSize
                checkable: true
                checked: controller.playbackMode === playbackMode
                onClicked: controller.playbackMode = playbackMode
                ButtonGroup.group: playbackModeGroup
            }
        }
        ToolSeparator {}
        IconButton {
            text: qsTr("Clear playlist")
            onClicked: model.clear()
            icon.source: "qrc:///images/trash.svg"
            size: soniqStyle.smallIconSize
            enabled: model.itemCount > 0
        }
    }
}
