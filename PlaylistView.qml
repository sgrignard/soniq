import QtQuick 2.15
import QtQuick.Layouts 1.15

ListView {
    required property var selectionModel
    property var onSongDoubleClicked: (song, index) => {}
    id: view
    clip: true
    highlightMoveDuration: 500
    highlightResizeDuration: 0
    currentIndex: view.selectionModel.currentIndex.row
    highlight: HighLightRectangle {}
    spacing: 10
    delegate: MouseArea {
        required property var song
        required property int index
        id: delegate
        onClicked: selectionModel.setCurrentRow(index)
        onDoubleClicked: view.onSongDoubleClicked(song, index)
        width: childrenRect.width
        height: childrenRect.height
        RowLayout {
            Image {
                source: api.coverArtUrl(song.coverArt, info.height)
                width: info.height
                height: info.height
            }
            SongInfo {
                id: info
                song: delegate.song
            }
        }
    }
}
