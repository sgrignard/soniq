#ifndef NETWORKACCESSMANAGERFACTORY_H
#define NETWORKACCESSMANAGERFACTORY_H

#include <QQmlNetworkAccessManagerFactory>


class QNetworkDiskCache;
struct Settings;

class NetworkAccessManagerFactory : public QQmlNetworkAccessManagerFactory
{
public:
    NetworkAccessManagerFactory();

    virtual QNetworkAccessManager* create(QObject* parent);
};

#endif // NETWORKACCESSMANAGERFACTORY_H
