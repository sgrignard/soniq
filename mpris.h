#ifndef MPRIS_H
#define MPRIS_H

#include <QDBusObjectPath>
#include <QObject>
#include <QPointer>

using StringVariantMap = QMap<QString,QVariant>;

class PlaylistController;

class MediaPlayer2: public QObject {
    Q_OBJECT
    // MediaPlayer2
    Q_PROPERTY(QString Identity READ identity)
    Q_PROPERTY(bool CanQuit READ canQuit)
    Q_PROPERTY(bool CanRaise READ canRaise)
    Q_PROPERTY(bool CanSetFullscreen READ canSetFullscreen)
    Q_PROPERTY(QString DesktopEntry READ desktopEntry)
    Q_PROPERTY(bool Fullscreen READ fullscreen WRITE setFullscreen)
    Q_PROPERTY(bool HasTrackList READ hasTrackList)
    Q_PROPERTY(QString Identity READ identity)
    Q_PROPERTY(QStringList SupportedMimeTypes READ supportedMimeTypes)
    Q_PROPERTY(QStringList SupportedUriSchemes READ supportedUriSchemes)

    // MediaPlayer2.Player
    Q_PROPERTY(QString LoopStatus READ loopStatus WRITE setLoopStatus NOTIFY loopStatusChanged)
    Q_PROPERTY(QString PlaybackStatus READ playbackStatus NOTIFY playbackStatusChanged)
    Q_PROPERTY(double Rate READ rate WRITE setRate NOTIFY rateChanged)
    Q_PROPERTY(bool Shuffle READ shuffle WRITE setShuffle NOTIFY shuffleChanged)
    Q_PROPERTY(double Volume READ volume WRITE setVolume)
    Q_PROPERTY(bool CanControl READ canControl)
    Q_PROPERTY(bool CanGoNext READ canGoNext)
    Q_PROPERTY(bool CanGoPrevious READ canGoPrevious)
    Q_PROPERTY(bool CanPause READ canPause)
    Q_PROPERTY(bool CanPlay READ canPlay)
    Q_PROPERTY(bool CanSeek READ canSeek)
    Q_PROPERTY(double MaximumRate READ maximumRate)
    Q_PROPERTY(double MinimumRate READ minimumRate)
    Q_PROPERTY(StringVariantMap Metadata READ metadata)
    Q_PROPERTY(qlonglong Position READ position)

public:

    MediaPlayer2(const QString& path, PlaylistController* controller);

    void setCoverArtBaseUrl(const QString& baseUrl);

    // MediaPlayer2
    QString identity() const;
    bool canQuit() const;
    bool canRaise() const;
    bool canSetFullscreen() const;
    bool fullscreen() const;
    QString desktopEntry() const;
    bool hasTrackList() const;
    QStringList supportedMimeTypes() const;
    QStringList supportedUriSchemes() const;

    // MediaPlayer2.Player
    QString loopStatus() const;
    QString playbackStatus() const;
    double rate() const;
    bool shuffle() const;
    bool canControl() const;
    bool canGoNext() const;
    bool canGoPrevious() const;
    bool canPause() const;
    bool canPlay() const;
    bool canSeek() const;
    double maximumRate() const;
    StringVariantMap metadata() const;
    double minimumRate() const;
    qlonglong position() const;
    double volume() const;

public slots:

    // MediaPlayer2
    void Quit();
    void Raise();
    void setFullscreen(bool fullscreen);

    // MediaPlayer2.Player
    void Next();
    void OpenUri(const QString &Uri);
    void Pause();
    void Play();
    void PlayPause();
    void Previous();
    void Seek(qlonglong Offset);
    void SetPosition(const QDBusObjectPath &TrackId, qlonglong Position);
    void Stop();
    void setLoopStatus(const QString &value);
    void setRate(double value);
    void setShuffle(bool value);
    void setVolume(double value);

signals:

    // MediaPlayer2.Player
    void loopStatusChanged(const QString& loopStatus);
    void playbackStatusChanged(const QString& PlaybackStatus);
    void rateChanged(double rate);
    void shuffleChanged(bool shuffle);

private:

    void notifyPropertyChanged(const QString& interface, const QString& propertyName);

    QString m_path;
    QPointer<PlaylistController> m_controller;
    QString m_coverArtBaseUrl;
};

#endif // MPRIS_H
