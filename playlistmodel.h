#ifndef PLAYLISTMODEL_H
#define PLAYLISTMODEL_H

#include <QAbstractTableModel>

#include "album.h"
#include "subsonicapi.h"

class QMediaPlayer;

class PlaylistModel : public QAbstractTableModel
{
    Q_OBJECT
    Q_PROPERTY(int itemCount READ itemCount NOTIFY itemCountChanged)
public:

    enum Roles {
        SongRole = Qt::UserRole + 1,
        PropertyNameRole,
        ColumnRole,
        RowRole
    };
    Q_ENUM(Roles)


    PlaylistModel(QObject* parent = nullptr);

    void setApi(SubsonicApi* api);

    virtual int rowCount(const QModelIndex&) const;

    virtual int columnCount(const QModelIndex&) const;

    virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;

    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

    virtual QHash<int, QByteArray> roleNames() const;

    Q_INVOKABLE int itemCount() const;

    Q_INVOKABLE Song song(int index) const;

    Q_INVOKABLE bool removeRows(int row, int count, const QModelIndex &parent);

    Q_INVOKABLE QModelIndex root() const;

    void setSongs(const SongList& songs);

public slots:

    void clear();

    void addAlbum(const QString& albumId);

signals:

    void itemCountChanged(int count);

private:

    QPointer<SubsonicApi> m_api;
    SongList m_songs;
};

#endif // PLAYLISTMODEL_H
