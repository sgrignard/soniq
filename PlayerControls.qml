import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import Soniq 1.0

RowLayout {
    id: buttons
    required property var player
    required property var controller
    IconButton {
        id: previousSong
        action: Action {
            enabled: controller.playlistPosition > 0
            text: qsTr("Previous")
            icon.source: "qrc:///images/step-previous.svg"
            onTriggered: controller.goPrevious()
            shortcut: "Alt+Left"
        }
    }
    IconButton {
        id: togglePlay
        action: Action {
            property bool playing: player.state === QMediaPlayer.PlayingState
            enabled: controller.songCount > 0
            text: playing ? qsTr("Pause") : qsTr("Play")
            icon.source: playing ? "qrc:///images/pause.svg" : "qrc:///images/play.svg"
            onTriggered: {
                if (controller.playlistPosition < 0) controller.goFirst()
                playing ? player.pause() : player.play()
            }
            shortcut: "P"
        }
    }
    IconButton {
        id: nextSong
        action: Action {
            enabled: controller.playlistPosition < controller.songCount - 1
            text: qsTr("Next")
            icon.source: "qrc:///images/step-next.svg"
            onTriggered: controller.goNext()
            shortcut: "Alt+Right"
        }
    }

    VolumeControl {
        player: buttons.player
        Layout.alignment: Qt.AlignRight
        Layout.preferredHeight: soniqStyle.iconSize
        Layout.preferredWidth: soniqStyle.iconSize
    }
}
